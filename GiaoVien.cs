﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asm1
{
    public class GiaoVien : NguoiLaoDong
    {
        private float HeSoLuong { get; set; }

        public GiaoVien() 
        {
            this.HeSoLuong = 1;
        }

        public GiaoVien(string hoTen, int namSinh, float luongCoBan, float heSoLuong) : base(hoTen, namSinh, luongCoBan)
        {
            this.HeSoLuong = heSoLuong;
        }

        /*public override GiaoVien NhapThongTin()
        {
            Console.WriteLine("Nhap ho ten:");
            string hoten = Console.ReadLine();
            Console.WriteLine("Nhap nam sinh:");
            int namsinh = (int)Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap luong co ban:");
            float luongcoban = (float)Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhap he so luong:");
            float hesoluong = (float)Convert.ToDouble(Console.ReadLine());
            GiaoVien giaovien = new GiaoVien(hoten, namsinh, luongcoban, hesoluong);
            return giaovien;

        }*/
        public void NhapThongTin(float heSoLuong)
        {
            this.HeSoLuong = heSoLuong;
        }
        public override float TinhLuong() => (float)(base.GetLuongCoBan * HeSoLuong * 1.25);
        public override void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine($" he so luong: {HeSoLuong}, luong: {TinhLuong()}");
        }
        public float XuLy()
        {
            return (float)(HeSoLuong + 0.6);
        }
    }
}
