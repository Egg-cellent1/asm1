﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace asm1
{
    public class NguoiLaoDong
    {
        private string HoTen { get; set; }
        private int NamSinh { get; set; }
        private float LuongCoBan { get; set; }
        protected float GetLuongCoBan => LuongCoBan;
        public NguoiLaoDong()
        {
            /*this.HoTen = string.Empty;
            this.NamSinh = 0;
            this.LuongCoBan = 0;*/
        }
        public NguoiLaoDong(string hoTen, int namSinh, float luongCoBan)
        {
            this.HoTen = hoTen;
            this.NamSinh = namSinh; 
            this.LuongCoBan = luongCoBan;
        }
        /*public virtual NguoiLaoDong NhapThongTin()
        {
            Console.WriteLine("Nhap ho ten:");
            string hoten = Console.ReadLine();
            Console.WriteLine("Nhap nam sinh:");
            int namsinh = (int)Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap luong co ban:");
            float luongcoban = (float)Convert.ToDouble(Console.ReadLine());
            NguoiLaoDong emp = new NguoiLaoDong(hoten, namsinh, luongcoban);
            return emp;
        }*/
        public void NhapThongTin(string  hoTen, int namSinh, float luongCoBan)
        {
            this.HoTen = hoTen;
            this.NamSinh = namSinh;
            this.LuongCoBan = luongCoBan;
        }
        public virtual float TinhLuong() => LuongCoBan;
        public virtual void XuatThongTin() => Console.WriteLine($"--Ho ten la:{HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}");

    }
}
