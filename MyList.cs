﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace asm1
{
    public class MyList
    {
        List<NguoiLaoDong> list;
        public MyList()
        {
            list = new List<NguoiLaoDong> ();
        }
        public void NhapThongTinList()
        {
            Console.WriteLine("Nhap so giao vien muon them");
            int num = (int)Convert.ToInt32(Console.ReadLine());

            if (num > 0)
            {
                
                for(int i = 0; i < num; i++)
                {
                    string hoten;
                    int namsinh;
                    float luongcoban, hesoluong;

                    do
                    {
                        Console.WriteLine("Nhap ho ten:");
                        hoten = Console.ReadLine();
                        if (!Regex.IsMatch(hoten, @"^[a-zA-Z\s]+$") || string.IsNullOrWhiteSpace(hoten)) Console.WriteLine("Invalid input");
                    } while (!Regex.IsMatch(hoten, @"^[a-zA-Z\s]+$") || string.IsNullOrWhiteSpace(hoten));

                    do
                    {
                        Console.WriteLine("Nhap nam sinh:");
                        try
                        {
                            namsinh = Convert.ToInt32(Console.ReadLine());
                            if (namsinh < 1900 || namsinh > 2099)
                            {
                                Console.WriteLine("Invalid input. Please enter a number from 1900 to 2099.");
                                continue;
                            }
                            break;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Invalid input. Please enter a number.");
                        }
                    } while (true);

                    do
                    {
                        Console.WriteLine("Nhap luong co ban:");
                        try
                        {
                            luongcoban = (float)Convert.ToDouble(Console.ReadLine());
                            if (luongcoban <= 0)
                            {
                                Console.WriteLine("Invalid input. Please enter a positive number.");
                                continue;
                            }
                            break;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Invalid input. Please enter a number.");
                        }
                    } while (true);

                    do
                    {
                        Console.WriteLine("Nhap he so luong:");
                        try
                        {
                            hesoluong = (float)Convert.ToDouble(Console.ReadLine());
                            if (hesoluong <= 0)
                            {
                                Console.WriteLine("Invalid input. Please enter a positive number.");
                                continue;
                            }
                            break;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Invalid input. Please enter a number.");
                        }
                    } while (true);

                    GiaoVien giaovien = new GiaoVien(hoten, namsinh, luongcoban, hesoluong);
                    list.Add(giaovien);
                    Console.WriteLine("Added");
                }  
            }

        }
        public void XuatThongTinList()
        {
            if (list.Count == 0) Console.WriteLine("Khong co giao vien trong danh sach");
            else
                foreach(NguoiLaoDong emp in list)
                {
                    emp.XuatThongTin();
                }
        }
        public void XuatThongTinLuongMin()
        {
            foreach (NguoiLaoDong a in list)
            {
                if (a.TinhLuong() <= list[0].TinhLuong())
                {
                    a.XuatThongTin();
                }
            }
        }
        public void seed5GiaoVien()
        {
            list.Add(new GiaoVien("mr a", 1950, 1000, (float)1.5));
            list.Add(new GiaoVien("mr b", 1950, 1000, (float)1.5));
            list.Add(new GiaoVien("mr c", 2003, 2000, (float)2));
            list.Add(new GiaoVien("mr d", 2003, 2000, (float)2));
            list.Add(new GiaoVien("mr e", 2010, 10000, (float)3));
            Console.WriteLine("Done!");
        }
    }
}
