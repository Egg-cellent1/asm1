﻿using System;

namespace asm1
{
    class Program
    {
        static void Main(string[] args)
        {
            MyList list = new MyList();

            while (true)
            {
                Console.WriteLine("----------Menu----------");
                Console.WriteLine("1. Nhap Thong Tin");
                Console.WriteLine("2. Danh sach giao vien");
                Console.WriteLine("3. Giao vien co luong thap nhat");
                Console.WriteLine("4. Seed 5 giao vien");
                Console.WriteLine("5. Thoat");

                Console.Write("Your choice: ");
                int choice = (int)Convert.ToInt32(Console.ReadLine());
                if (choice == 5) break;
                switch (choice)
                {
                    case 1:
                        list.NhapThongTinList();
                        break;
                    case 2:
                        list.XuatThongTinList();
                        break;
                    case 3:
                        list.XuatThongTinLuongMin();
                        break;
                    case 4:
                        list.seed5GiaoVien();
                        break;
                    default:
                        Console.WriteLine("Ko co muc tuong ung");
                        break;
                }
            } 

        }
    }
}
